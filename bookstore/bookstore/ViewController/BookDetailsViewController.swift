//
//  BookDetailsViewController.swift
//  bookstore
//
//  Created by Yin Lai on 02/12/2022.
//

import UIKit

//protocol BookDetailsDataEnteredDelegate{
//    func userDidEnterBookDetailsInformation(bdImgInfo:UIImage, bdNameInfo:NSString, bdAuthInfo:NSString, bdDescInfo:NSString)
//}
protocol UpdateBooklist {
    func updateBooklist(book:Book, row:Int)
}

class BookDetailsViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameTF: UITextField!
    
    @IBOutlet weak var authorTF: UITextField!
    
    @IBOutlet weak var desTF: UITextField!
    
    var imagePicker = UIImagePickerController()
    var activeTextField: UITextField? = nil
    
    var detailImg: UIImage = UIImage()
    var detailName: String = ""
    var detailAuthor: String = ""
    var detailDesc: String = ""
    
    
    var delegate:UpdateBooklist? = nil

    
//    var titleText: String?
//    var detailText: String?
//    var authorText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGray2
        navigationItem.hidesBackButton = true
        navView()
        imgClickAction()
        
        imgView.isUserInteractionEnabled = false
        nameTF.isEnabled = false
        authorTF.isEnabled = false
        desTF.isEnabled = false
        
        imgView.image = detailImg
        nameTF.text = detailName
        authorTF.text = detailAuthor
        desTF.text = detailDesc
        
//        nameTF.text = titleText
//        authorTF.text = authorText
//        desTF.text = detailText
    }
    
    func navView(){
        
        let editBtn = UIButton()
        editBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        editBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        editBtn.setTitleColor(.white, for: .normal)
        editBtn.backgroundColor = .black
        editBtn.layer.cornerRadius = 15
        editBtn.setTitle("Edit", for: .normal)
        editBtn.tag = 3000
        editBtn.setTitle("Save", for: .selected)
        editBtn.addTarget(self, action: #selector(editBtnSavedAction(btn:)), for: .touchUpInside)
        print("save btn click")
    
        
        let backBtn = UIButton()
        backBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        backBtn.setTitle("Back", for: .normal)
        backBtn.setTitleColor(.white, for: .normal)
        backBtn.backgroundColor = .black
        backBtn.layer.cornerRadius = 15
        backBtn.addTarget(self, action: #selector(backBtnAction), for: .touchUpInside)
        
        let titleLabel = UILabel()
        titleLabel.text = "Edit Book"
        titleLabel.textColor = .black
        titleLabel.adjustsFontSizeToFitWidth = true
        
        let stackView = UIStackView(arrangedSubviews: [backBtn, titleLabel, editBtn])
        stackView.spacing = 80
        stackView.alignment = .center
        
        
        // This will assing your custom view to navigation title.
        navigationItem.titleView = stackView
    }
    
    @objc func editBtnSavedAction(btn:UIButton){
        
        if (btn.isSelected){
            
            if (nameTF.text == "" && authorTF.text == "" && desTF.text == "" && imgView.image == nil){
                let alert = UIAlertController(title: "Fields cannot be empty", message: "", preferredStyle: .alert)
                present(alert, animated: true, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    alert.dismiss(animated: true, completion: nil)
                }
                return
            }else if (nameTF.text == "" || nameTF.text!.starts(with: " ")){
                let alert = UIAlertController(title: "Name field cannot be empty", message: "Please remove any spaces in front of the sentence or fill in the field", preferredStyle: .alert)
                present(alert, animated: true, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    alert.dismiss(animated: true, completion: nil)
                }
                return
            }else if (authorTF.text == "" || authorTF.text!.starts(with: " ")){
                let alert = UIAlertController(title: "Author field cannot be empty", message: "Please remove any spaces in front of the sentence or fill in the field", preferredStyle: .alert)
                present(alert, animated: true, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    alert.dismiss(animated: true, completion: nil)
                }
                return
            }else if (desTF.text == "" || desTF.text!.starts(with: " ")){
                let alert = UIAlertController(title: "Description field cannot be empty", message: "Please remove any spaces in front of the sentence or fill in the field", preferredStyle: .alert)
                present(alert, animated: true, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    alert.dismiss(animated: true, completion: nil)
                }
                return
            }else if (imgView.image == nil){
                let alert = UIAlertController(title: "Image field cannot be empty", message: "", preferredStyle: .alert)
                present(alert, animated: true, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    alert.dismiss(animated: true, completion: nil)
                }
                return
            } else{
                
                var book = Book(bookImage: imgView.image!, bookTitle: nameTF.text ?? " ", bookAuth: authorTF.text ?? " ", bookDescrip: desTF.text ?? " ")
                self.delegate?.updateBooklist(book: book, row:self.view.tag)
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        btn.isSelected = !btn.isSelected
        imgView.isUserInteractionEnabled = btn.isSelected
        nameTF.isEnabled = btn.isSelected
        authorTF.isEnabled = btn.isSelected
        desTF.isEnabled = btn.isSelected
        
    }
    
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func imgClickAction(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))

        imgView.addGestureRecognizer(tap)

        imgView.isUserInteractionEnabled = true

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        print("img clicked")
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            
            self.present(alert, animated: true, completion: nil)
    }

    //MARK: - Open the camera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose image from camera roll
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func registerForKeyboardNotifications() {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    
    @IBAction  func keyboardWillShow(_ notification: NSNotification) {
       guard let userInfo = notification.userInfo,
             let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        var shoulMoveViewUp = false
        
        if let activeTextField = activeTextField {
            
            let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY
            let topOfKeyboard = self.view.frame.height - keyboardScreenEndFrame.height
            
            if bottomOfTextField > topOfKeyboard {
                shoulMoveViewUp = true
            }
        }
        
        if shoulMoveViewUp {
           if notification.name == UIResponder.keyboardWillShowNotification {
//               topConstraint.constant -= keyboardScreenEndFrame.height
//               bottomConstraint.constant = keyboardScreenEndFrame.height
           }

        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
       
   }
}

//MARK: - UIImagePickerControllerDelegate

extension BookDetailsViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            self.imgView.image = editedImage
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
}

extension BookDetailsViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.activeTextField = nil
//        topConstraint.constant = 0
//        bottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
}


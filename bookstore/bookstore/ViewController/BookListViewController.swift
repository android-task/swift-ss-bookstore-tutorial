//
//  BookListCVController.swift
//  bookstore
//
//  Created by Yin Lai on 02/12/2022.
//

import UIKit
import CoreData

struct Book {
    let bookImage: UIImage
    let bookTitle: String
    let bookAuth: String
    let bookDescrip: String
    
}
class BookListViewController: UIViewController, DataEnteredDelegate, UpdateBooklist {
    func updateBooklist(book: Book, row: Int) {
        if (row == -1){return}
        let indexPath = IndexPath(item: row, section: 0)
        bookList[row] = book
        
        bookTableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    
    
    @IBOutlet weak var bookTableView: UITableView!
    
    private var bookDetailsViewController : BookDetailsViewController!
    private var createBookViewController : CreateNewBookViewController!
    
    public var bookList: [Book] = []
    
    //from edited book detail
//    var edittedBookImg = UIImage()
//    var edittedBookName = ""
//    var edittedBookAuth = ""
//    var edittedBookDesc = ""
    
    var lastIndex:IndexPath!
    
    // Core data context
    public var context: NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGray2
        navigationItem.hidesBackButton = true
        navView()
        
        bookTableView.register(BookTableViewCell.self, forCellReuseIdentifier: "cell")
        bookTableView.delegate = self
        bookTableView.dataSource = self
        bookTableView.backgroundColor = .clear
        bookTableView.reloadData()
        
        
    }
    
    func userDidEnterInformation(imageInfo: UIImage, info: NSString, info2: NSString, info3: NSString) {
        bookList.append(Book(bookImage: imageInfo, bookTitle: info as String, bookAuth: info2 as String, bookDescrip: info3 as String))
        
        bookTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateNewBookViewController"{
            let newBookVC:CreateNewBookViewController = segue.destination as! CreateNewBookViewController
            newBookVC.delegate = self
        }
        
        if (segue.identifier == "BookDetailsViewController")
        {
            
        }
    }
    
    func navView(){
        
        let addBtn = UIButton()
        addBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        addBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addBtn.setTitle("+", for: .normal)
        addBtn.setTitleColor(.white, for: .normal)
        addBtn.backgroundColor = .black
        addBtn.layer.cornerRadius = 15
        addBtn.addTarget(self, action: #selector(addBtnAction), for: .touchUpInside)
        
        let backBtn = UIButton()
        backBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        backBtn.setTitle("Back", for: .normal)
        backBtn.setTitleColor(.white, for: .normal)
        backBtn.backgroundColor = .black
        backBtn.layer.cornerRadius = 15
        backBtn.addTarget(self, action: #selector(backBtnAction), for: .touchUpInside)
        
        let titleLabel = UILabel()
        titleLabel.text = "SS BOOKSTORE"
        titleLabel.textColor = .black
        titleLabel.adjustsFontSizeToFitWidth = true
        
        let stackView = UIStackView(arrangedSubviews: [backBtn, titleLabel, addBtn])
        stackView.spacing = 60
        stackView.alignment = .center
        
        
        // This will assing your custom view to navigation title.
        navigationItem.titleView = stackView
    }
    
    @objc func addBtnAction(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let createBookVC = storyBoard.instantiateViewController(withIdentifier: "CreateNewBookViewController") as! CreateNewBookViewController
        createBookVC.delegate = self
        self.navigationController?.pushViewController(createBookVC, animated: true)
        
    }
    
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
        
    }
}

// MARK: - Extension for tableview methods
extension BookListViewController: UITableViewDelegate, UITableViewDataSource{
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookTableViewCell", for: indexPath) as! BookTableViewCell
        
        let book = bookList[indexPath.row]
        cell.bookImgView.image = book.bookImage
        cell.bookNameLbl?.text = book.bookTitle
        cell.bookAuthorLbl?.text = book.bookAuth
        cell.bookDescLbl?.text = book.bookDescrip
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        debugPrint("didselect = \(indexPath.row)")
        
        lastIndex = indexPath
        let cell = bookTableView.cellForRow(at: indexPath) as! BookTableViewCell
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let details = storyboard.instantiateViewController(identifier: "BookDetailsViewController") as? BookDetailsViewController else { return }
        details.view.tag = indexPath.row
        
        let book = bookList[indexPath.row]
        details.modalPresentationStyle = .fullScreen
        details.delegate = self
        details.loadViewIfNeeded()
        details.nameTF.text = cell.bookNameLbl.text!
        details.authorTF.text = cell.bookAuthorLbl.text!
        details.desTF.text = book.bookDescrip
        details.imgView.image = cell.bookImgView.image!
        
        self.show(details, sender: nil)
        
        bookTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.bookList.remove(at: indexPath.row)
            self.bookTableView.deleteRows(at: [indexPath], with: .automatic)
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

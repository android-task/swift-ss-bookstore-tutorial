//
//  CreateNewBookViewController.swift
//  bookstore
//
//  Created by Yin Lai on 05/12/2022.
//

import UIKit

protocol DataEnteredDelegate{
    func userDidEnterInformation(imageInfo:UIImage, info:NSString, info2:NSString, info3:NSString)
}

class CreateNewBookViewController: UIViewController {
    
    private var booklistViewController : BookListViewController!
    
    var delegate:DataEnteredDelegate? = nil
    
//    @IBOutlet weak var imgView: UIImageView!
//    @IBOutlet weak var nameTF: UITextField!
//    @IBOutlet weak var authorTF: UITextField!
//    @IBOutlet weak var desTF: UITextField!
    
    @IBOutlet weak var addImgView: UIImageView!
    @IBOutlet weak var addNameTF: UITextField!
    @IBOutlet weak var addAuthorTF: UITextField!
    @IBOutlet weak var addDescTF: UITextField!
    
    var imagePicker = UIImagePickerController()
    
    var activeTextField: UITextField? = nil
    
    
    //    var offsetY:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGray2
        navigationItem.hidesBackButton = true
        navView()
        imgClickAction()
        
        addNameTF.delegate = self
        addAuthorTF.delegate = self
        addDescTF.delegate = self
        
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardFrameChangeNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    //    }
    //
    //    @objc func keyboardFrameChangeNotification(notification: Notification) {
    //        if let userInfo = notification.userInfo {
    //            let endFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
    //            let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0
    //            let animationCurveRawValue = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int) ?? Int(UIView.AnimationOptions.curveEaseInOut.rawValue)
    //            let animationCurve = UIView.AnimationOptions(rawValue: UInt(animationCurveRawValue))
    //            if let _ = endFrame, endFrame!.intersects(self.desTF.frame) {
    //                self.offsetY = self.desTF.frame.maxY - endFrame!.minY
    //                UIView.animate(withDuration: animationDuration, delay: TimeInterval(0), options: animationCurve, animations: {
    //                    self.desTF.frame.origin.y = self.desTF.frame.origin.y - self.offsetY
    //                }, completion: nil)
    //            } else {
    //                if self.offsetY != 0 {
    //                    UIView.animate(withDuration: animationDuration, delay: TimeInterval(0), options: animationCurve, animations: {
    //                        self.desTF.frame.origin.y = self.desTF.frame.origin.y + self.offsetY
    //                        self.offsetY = 0
    //                    }, completion: nil)
    //                }
    //            }
    //        }
    //    }
    
    
    func navView(){
        
        let saveBtn = UIButton()
        saveBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        saveBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        saveBtn.setTitle("Save", for: .normal)
        saveBtn.setTitleColor(.white, for: .normal)
        saveBtn.backgroundColor = .black
        saveBtn.layer.cornerRadius = 15
        saveBtn.addTarget(self, action: #selector(saveBtnAction), for: .touchUpInside)
        
        let backBtn = UIButton()
        backBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backBtn.widthAnchor.constraint(equalToConstant: 60).isActive = true
        backBtn.setTitle("Back", for: .normal)
        backBtn.setTitleColor(.white, for: .normal)
        backBtn.backgroundColor = .black
        backBtn.layer.cornerRadius = 15
        backBtn.addTarget(self, action: #selector(backBtnAction), for: .touchUpInside)
        
        let titleLabel = UILabel()
        titleLabel.text = "Create Book"
        titleLabel.textColor = .black
        titleLabel.adjustsFontSizeToFitWidth = true
        
        let stackView = UIStackView(arrangedSubviews: [backBtn, titleLabel, saveBtn])
        stackView.spacing = 80
        stackView.alignment = .center
        
        
        // This will assing your custom view to navigation title.
        navigationItem.titleView = stackView
    }
    
    @objc func saveBtnAction(){
        if (addNameTF.text == "" && addAuthorTF.text == "" && addDescTF.text == "" && addImgView.image == nil){
            let alert = UIAlertController(title: "Fields cannot be empty", message: "", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else if (addNameTF.text == "" || addNameTF.text!.starts(with: " ")){
            let alert = UIAlertController(title: "Name field cannot be empty", message: "Please remove any spaces in front of the sentence or fill in the field", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else if (addAuthorTF.text == "" || addAuthorTF.text!.starts(with: " ")){
            let alert = UIAlertController(title: "Author field cannot be empty", message: "Please remove any spaces in front of the sentence or fill in the field", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else if (addDescTF.text == "" || addDescTF.text!.starts(with: " ")){
            let alert = UIAlertController(title: "Description field cannot be empty", message: "Please remove any spaces in front of the sentence or fill in the field", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else if (addImgView.image == nil){
            let alert = UIAlertController(title: "Image field cannot be empty", message: "", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else {
            
            let imgInfo:UIImage = addImgView.image! as UIImage
            let nameInfo:NSString = addNameTF.text! as NSString
            let authorInfo:NSString = addAuthorTF.text! as NSString
            let descInfo:NSString = addDescTF.text! as NSString
            
            delegate?.userDidEnterInformation(imageInfo: imgInfo, info: nameInfo, info2: authorInfo, info3: descInfo)
            
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func imgClickAction(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        addImgView.addGestureRecognizer(tap)
        
        addImgView.isUserInteractionEnabled = true
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        print("img clicked")
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction  func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        var shoulMoveViewUp = false
        
        if let activeTextField = activeTextField {
            
            let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY
            let topOfKeyboard = self.view.frame.height - keyboardScreenEndFrame.height
            
            if bottomOfTextField > topOfKeyboard {
                shoulMoveViewUp = true
            }
        }
        
        if shoulMoveViewUp {
            if notification.name == UIResponder.keyboardWillShowNotification {
                //               topConstraint.constant -= keyboardScreenEndFrame.height
                //               bottomConstraint.constant = keyboardScreenEndFrame.height
            }
            
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    //MARK: - Open the camera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose image from camera roll
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
}

//MARK: - UIImagePickerControllerDelegate

extension CreateNewBookViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            self.addImgView.image = editedImage
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
}

extension CreateNewBookViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
//        self.addNameTF = textField
//        self.addAuthorTF = textField
//        self.addDescTF = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.activeTextField = nil
        //        topConstraint.constant = 0
        //        bottomConstraint.constant = 0
//        self.addNameTF = nil
//        self.addAuthorTF = nil
//        self.addDescTF = nil
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

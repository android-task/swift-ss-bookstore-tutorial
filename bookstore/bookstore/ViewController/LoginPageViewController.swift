//
//  LoginPageView.swift
//  bookstore
//
//  Created by Yin Lai on 22/11/2022.
//

import UIKit

class LoginPageViewController: UIViewController {

    private var booklistViewController : BookListViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGray2
        
        setUpSubviews()
        setUpConstraints()
        
        loginBtn.addTarget(self, action: #selector(loginBtnAction), for: .touchUpInside)
    }
    
    func setUpSubviews(){
        view.addSubview(imgView)
        view.addSubview(txtView)
        view.addSubview(userName)
        view.addSubview(password)
        view.addSubview(loginBtn)
    }

    func setUpConstraints(){
        imgView.frame = CGRect(x: view.frame.midX - 75, y: (view.frame.minY) + 150, width: 150, height: 150)
        
        txtView.translatesAutoresizingMaskIntoConstraints = false
        txtView.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 20).isActive = true
        txtView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 30).isActive = true
        txtView.widthAnchor.constraint(equalToConstant: view.frame.size.width - 60).isActive = true
        txtView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        userName.translatesAutoresizingMaskIntoConstraints = false
        userName.topAnchor.constraint(equalTo: txtView.bottomAnchor, constant: 20).isActive = true
        userName.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 100).isActive = true
        userName.widthAnchor.constraint(equalToConstant: view.frame.size.width - 200).isActive = true
        userName.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        password.translatesAutoresizingMaskIntoConstraints = false
        password.topAnchor.constraint(equalTo: userName.bottomAnchor, constant: 20).isActive = true
        password.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 100).isActive = true
        password.widthAnchor.constraint(equalToConstant: view.frame.size.width - 200).isActive = true
        password.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        loginBtn.translatesAutoresizingMaskIntoConstraints = false
        loginBtn.topAnchor.constraint(equalTo: password.bottomAnchor, constant: 20).isActive = true
        loginBtn.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 100).isActive = true
        loginBtn.widthAnchor.constraint(equalToConstant: view.frame.size.width - 200).isActive = true
        loginBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    @objc func loginBtnAction(){
        if (userName.text == "" && password.text == ""){
            let alert = UIAlertController(title: "Fields cannot be empty", message: "", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else if (userName.text == ""){
            let alert = UIAlertController(title: "Name field cannot be empty", message: "", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else if (password.text == ""){
            let alert = UIAlertController(title: "Password field cannot be empty", message: "", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else if (userName.text != "SS" || password.text != "11111"){
            let alert = UIAlertController(title: "Login info invalid", message: "", preferredStyle: .alert)
            present(alert, animated: true, completion: nil)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alert.dismiss(animated: true, completion: nil)
            }
        }else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let booklistVC = storyBoard.instantiateViewController(withIdentifier: "BookListViewController") as! BookListViewController
            self.navigationController?.pushViewController(booklistVC, animated: true)
        }
        
    }
    
    private let imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(systemName: "book")
        imgView.contentMode = .scaleAspectFit
        imgView.tintColor = .black
        
        return imgView
    }()
    
    private let txtView: UITextView = {
        let txtView = UITextView()
        txtView.text = Constants.LoginPage.welcomeTxt
        txtView.textColor = .black
        txtView.font = .boldSystemFont(ofSize: 14)
        txtView.isSelectable = false
        txtView.textAlignment = .center
        txtView.backgroundColor = .clear
        
        return txtView
    }()
    
    private let userName: UITextField = {
        let userName = UITextField()
        userName.placeholder = Constants.LoginPage.userName
        userName.font = .systemFont(ofSize: 14)
        userName.backgroundColor = .white
        userName.textAlignment = .center
        userName.layer.borderWidth = 1
        userName.textColor = .black
        
        return userName
    }()
    
    private let password: UITextField = {
        let password = UITextField()
        password.placeholder = Constants.LoginPage.password
        password.font = .systemFont(ofSize: 14)
        password.backgroundColor = .white
        password.textAlignment = .center
        password.isSecureTextEntry = true
        password.layer.borderWidth = 1
        password.textColor = .black
        
        return password
    }()
    
    private let loginBtn: UIButton = {
        let loginBtn = UIButton()
        loginBtn.setTitle(Constants.LoginPage.login, for: .normal)
        loginBtn.setTitleColor(.white, for: .normal)
        loginBtn.backgroundColor = .black
        
        return loginBtn
    }()

    
}


//
//  BookTableViewCell.swift
//  bookstore
//
//  Created by Yin Lai on 03/12/2022.
//

import UIKit

class BookTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bookNameLbl: UILabel!
    @IBOutlet weak var bookAuthorLbl: UILabel!
    var bookDescLbl: UILabel!
    @IBOutlet weak var bookImgView: UIImageView!
    @IBOutlet weak var view: UIView!
    
    private var bookDetailsViewController : BookDetailsViewController!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }

}

//
//  Constants.swift
//  bookstore
//
//  Created by Yin Lai on 01/12/2022.
//

import Foundation

struct Constants{
    
    struct LoginPage{
        static let welcomeTxt : String = "Hello! Welcome to SS BookStore"
        static let userName : String = "USERNAME"
        static let password : String = "PASSWORD"
        static let login : String = "LOGIN"
    }
    
}
